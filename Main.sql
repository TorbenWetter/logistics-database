CREATE DATABASE IF NOT EXISTS `Team4` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `Team4`;

--
-- Table structure for table `carrier`
--

DROP TABLE IF EXISTS `carrier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `carrier` (
  `carrier_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `phone` varchar(31) COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`carrier_id`),
  UNIQUE KEY `Transporteur_Id_UNIQUE` (`carrier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='gefahrene Transporte: Liste von allen bereits gefaherenen Transporten';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carrier`
--

LOCK TABLES `carrier` WRITE;
/*!40000 ALTER TABLE `carrier` DISABLE KEYS */;
INSERT INTO `carrier` VALUES (1,'Peter Müller','+49 172 4584 533'),(2,'Hans Schmidt','+49 1688 345467'),(3,'Paul Franzen','+49 145 456 972'),(4,'Peter Parker','+1 336 6790 2'),(5,'Zoe Evans','+1 856 278 04'),(6,'Axel Bauer','+49 1587 356 7'),(7,'Tom Fleiss','+49 4566 77 8'),(8,'Gregor Nie','+49 332 345 6');
/*!40000 ALTER TABLE `carrier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_type`
--

DROP TABLE IF EXISTS `order_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_type` (
  `order_type_id` int NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `min_weight` int DEFAULT NULL,
  `max_weight` int NOT NULL,
  `price` float NOT NULL,
  `min_length` float NOT NULL,
  `max_length` float NOT NULL,
  `min_width` float NOT NULL,
  `max_width` float NOT NULL,
  `min_height` float DEFAULT NULL,
  `max_height` float DEFAULT NULL,
  PRIMARY KEY (`order_type_id`),
  UNIQUE KEY `OrderType_Id_UNIQUE` (`order_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_type`
--

LOCK TABLES `order_type` WRITE;
/*!40000 ALTER TABLE `order_type` DISABLE KEYS */;
INSERT INTO `order_type` VALUES (1,'Postkarte',150,500,0.6,14,23.5,9,12.5,NULL,NULL),(2,'Standardbrief',NULL,20,0.8,14,23.5,9,12.5,NULL,0.5),(3,'Kompaktbrief',NULL,50,0.95,10,23.5,7,12.5,NULL,1),(4,'Großbrief',NULL,500,1.55,10,35.3,7,25,NULL,2),(5,'Maxibrief',NULL,1000,2.7,10,35.3,7,25,NULL,5),(6,'Paeckchen S',NULL,2000,3.79,15,35,11,25,1,10),(7,'Paeckchen M',NULL,2000,4.39,15,60,11,30,1,15),(8,'Leichtpaket',NULL,5000,5.99,15,120,11,60,1,60),(9,'Paket',NULL,10000,8.49,15,120,11,60,1,60),(10,'Schwerpaket',NULL,31500,16.08,15,120,11,60,1,60);
/*!40000 ALTER TABLE `order_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product` (
  `product_id` int NOT NULL AUTO_INCREMENT,
  `order_type_id` int NOT NULL,
  `sender` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `receiver` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `weight` float NOT NULL,
  `length` float NOT NULL,
  `width` float NOT NULL,
  `height` float DEFAULT NULL,
  PRIMARY KEY (`product_id`),
  UNIQUE KEY `Produkt_Id_UNIQUE` (`product_id`),
  KEY `OrderType_Id_idx` (`order_type_id`),
  CONSTRAINT `order_type_id` FOREIGN KEY (`order_type_id`) REFERENCES `order_type` (`order_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,1,'Laura Blechschmidt','Torben Wetter',300,23.5,12.5,0),(2,6,'Julian Withelm','Joerg Schaede',1500,32,20,6),(3,4,'Torben Wetter','Deutsche Bahn',180,0.1,35.5,25),(4,8,'Barack Obama','Joerg Schaede',3000,100,40,40);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `station`
--

DROP TABLE IF EXISTS `station`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `station` (
  `station_id` int NOT NULL AUTO_INCREMENT,
  `station_type_id` int NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`station_id`),
  UNIQUE KEY `Station_Id_UNIQUE` (`station_id`),
  KEY `Stationstyp_Id_idx` (`station_type_id`),
  CONSTRAINT `station_type_id` FOREIGN KEY (`station_type_id`) REFERENCES `station_type` (`station_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Lagerbestand: Die in diesem Moment in dieser Station  gelagerten Produkte';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `station`
--

LOCK TABLES `station` WRITE;
/*!40000 ALTER TABLE `station` DISABLE KEYS */;
INSERT INTO `station` VALUES (1,3,'Zornheimer Poststelle','Konrad-Adenauer-Straße 24, 55270 Zornheim'),(2,1,'Postfiliale 501','Bahnhofsstraße 2, 55116 Mainz'),(3,4,'Torben Wetter','Am Hipperich 63, 55120 Mainz'),(4,4,'Barack Obama','Big Road 69, 10022 New York City'),(5,1,'Postfiliale NYC 221','JFK Street 3, 10028 New York City'),(6,5,'Hafenlager NYC 34','Little Place 1, 10023 New York City'),(7,5,'Hafenlager HH 65','Am Hafen 45, 20457 Hamburg'),(8,2,'Postlager HH ','Alte Fabrik 5, 20458 Hamburg'),(9,2,'Postlager M ','Finkenstraße 32, 55116 Mainz'),(10,3,'Poststation M 3','Rosengasse 2, 55116 Mainz'),(11,1,'Postfiliale 520','Hechtsheimer Straße 7, 55131 Mainz'),(12,4,'Joerg Schaede','Bekannte Straße 2, 55131 Mainz');
/*!40000 ALTER TABLE `station` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `station_type`
--

DROP TABLE IF EXISTS `station_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `station_type` (
  `station_type_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `capacity` int DEFAULT NULL,
  PRIMARY KEY (`station_type_id`),
  UNIQUE KEY `Stationstyp_Id_UNIQUE` (`station_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='Kapazität: bis zu ... lagerfähig';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `station_type`
--

LOCK TABLES `station_type` WRITE;
/*!40000 ALTER TABLE `station_type` DISABLE KEYS */;
INSERT INTO `station_type` VALUES (1,'Postfiliale',350),(2,'Lager',2000),(3,'Poststation',350),(4,'Kunde',NULL),(5,'Hafenlager',2500);
/*!40000 ALTER TABLE `station_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transport`
--

DROP TABLE IF EXISTS `transport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transport` (
  `transport_id` int NOT NULL AUTO_INCREMENT,
  `product_id` int NOT NULL,
  `transport_vehicle_id` int NOT NULL,
  `carrier_id` int NOT NULL,
  `start_station_id` int NOT NULL,
  `end_station_id` int NOT NULL,
  `start_date` timestamp NOT NULL,
  `end_date` timestamp NOT NULL,
  PRIMARY KEY (`transport_id`),
  UNIQUE KEY `Transport_id_UNIQUE` (`transport_id`),
  KEY `Transporteur_id_idx` (`carrier_id`),
  KEY `Produkt_id_idx` (`product_id`),
  KEY `Ziel_id_idx` (`end_station_id`),
  KEY `Start_id_idx` (`start_station_id`),
  KEY `Transportmittel_Id_idx` (`transport_vehicle_id`),
  CONSTRAINT `carrier_id` FOREIGN KEY (`carrier_id`) REFERENCES `carrier` (`carrier_id`),
  CONSTRAINT `end_station_id` FOREIGN KEY (`end_station_id`) REFERENCES `station` (`station_id`),
  CONSTRAINT `product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`),
  CONSTRAINT `start_station_id` FOREIGN KEY (`start_station_id`) REFERENCES `station` (`station_id`),
  CONSTRAINT `transport_vehicle_id` FOREIGN KEY (`transport_vehicle_id`) REFERENCES `transport_vehicle` (`transport_vehicle_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transport`
--

LOCK TABLES `transport` WRITE;
/*!40000 ALTER TABLE `transport` DISABLE KEYS */;
INSERT INTO `transport` VALUES (1,1,6,2,1,2,'2020-10-20 06:20:00','2020-10-22 07:43:00'),(2,1,4,1,2,3,'2020-10-22 08:10:00','2020-10-22 10:31:00'),(3,4,5,4,5,6,'2020-10-31 11:31:00','2020-10-31 14:11:00'),(4,4,2,5,6,7,'2020-10-31 17:45:00','2020-11-10 06:34:00'),(5,4,7,3,7,8,'2020-11-10 08:50:00','2020-11-10 12:02:00'),(6,4,3,6,8,9,'2020-11-11 07:14:00','2020-11-11 19:09:00'),(7,4,4,1,9,10,'2020-11-12 05:56:00','2020-11-12 07:40:00'),(8,4,8,7,10,11,'2020-11-12 08:03:00','2020-11-12 11:56:00'),(9,4,9,8,11,12,'2020-11-13 06:16:00','2020-11-13 08:01:00');
/*!40000 ALTER TABLE `transport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transport_type`
--

DROP TABLE IF EXISTS `transport_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transport_type` (
  `transport_type_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `capacity` int NOT NULL,
  PRIMARY KEY (`transport_type_id`),
  UNIQUE KEY `id_Transportart_UNIQUE` (`transport_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transport_type`
--

LOCK TABLES `transport_type` WRITE;
/*!40000 ALTER TABLE `transport_type` DISABLE KEYS */;
INSERT INTO `transport_type` VALUES (1,'Flugzeug',10000),(2,'Schiff',30000),(3,'Zug',5000),(4,'LKW',2000),(5,'Auto',400);
/*!40000 ALTER TABLE `transport_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transport_vehicle`
--

DROP TABLE IF EXISTS `transport_vehicle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transport_vehicle` (
  `transport_vehicle_id` int NOT NULL AUTO_INCREMENT,
  `transport_type_id` int NOT NULL,
  `label` varchar(45) COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`transport_vehicle_id`),
  UNIQUE KEY `Transportmittel_Id_UNIQUE` (`transport_vehicle_id`),
  KEY `Transporttyp_Id_idx` (`transport_type_id`),
  CONSTRAINT `transport_type_id` FOREIGN KEY (`transport_type_id`) REFERENCES `transport_type` (`transport_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transport_vehicle`
--

LOCK TABLES `transport_vehicle` WRITE;
/*!40000 ALTER TABLE `transport_vehicle` DISABLE KEYS */;
INSERT INTO `transport_vehicle` VALUES (1,1,'FL 34'),(2,2,'SC 45-6'),(3,4,'L 556'),(4,5,'MZ A 1203'),(5,5,'NYC PS 44'),(6,5,'MZ TR 102'),(7,5,'HH M 3332'),(8,5,'MZ GU 49'),(9,5,'MZ P 4576');
/*!40000 ALTER TABLE `transport_vehicle` ENABLE KEYS */;
UNLOCK TABLES;
